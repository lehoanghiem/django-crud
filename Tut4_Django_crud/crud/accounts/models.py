# from django.conf import settings
from django.db import models


# Create your models here.
class Employee(models.Model):
    # user = models.OneToOneField(settings.AUTH_USER_MODEL)
    email = models.EmailField(max_length=60, unique=True, db_index=True)
    username = models.CharField(max_length=60, unique=True, db_index=True)

    first_name = models.CharField(max_length=60, db_index=True)
    last_name = models.CharField(max_length=60, db_index=True)
    # password_hash

    is_admin = models.BooleanField(default=False)

    def __str__(self):
        return '{} {}'.format(self.user.username, self.email)
