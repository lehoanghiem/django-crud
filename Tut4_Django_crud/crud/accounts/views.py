from django.contrib.auth import login, get_user_model, logout, authenticate
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from .forms import UserLoginForm, UserRegisterForm
from django.contrib.auth.decorators import login_required

# Create your views here.
User = get_user_model()


def index(request, *args, **kwargs):
    return render(request, "base.html", {})


@login_required
def home(request, *args, **kwargs):
    return render(request, "home.html", {})


def register(request, *args, **kwargs):
    form = UserRegisterForm(request.POST or None)
    # After user registered, the web application will retrieve
    # into the 'login' page
    if form.is_valid():
        form.save()
        return HttpResponseRedirect("/login")
    return render(request, "accounts/register.html", {"form": form})


def user_login(request, *args, **kwargs):
    form = UserLoginForm(request.POST or None)
    if form.is_valid():
        # form.save()
        username_ = form.cleaned_data.get('username')
        user_obj = User.objects.get(username__iexact=username_)
        login(request, user_obj)
        return HttpResponseRedirect("/home")
    return render(request, "accounts/login.html", {"form": form})


def user_logout(request, *args, **kwargs):
    logout(request)
    return HttpResponseRedirect("/login")
