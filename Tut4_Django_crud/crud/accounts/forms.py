# App 'accounts' contains the forms and views of
# login
# logout
# registration

from django import forms
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.contrib.auth import (
    authenticate,
    get_user_model,
    login,
    logout,
)
from django.core.validators import RegexValidator

USERNAME_REGEX = '^[a-zA-Z0-9.@+-]*$'

User = get_user_model()


class UserLoginForm(forms.Form):
    username = forms.CharField(label='Username', validators=[RegexValidator(
                        regex=USERNAME_REGEX,
                        message='Username must be Alphanumeric or contain any of the following: ". @ + -" ',
                        code='invalid_username'
                    )])
    password = forms.CharField(label='Password', widget=forms.PasswordInput)

    def clean(self, *args, **kwargs):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        # Approach 1: Checking authentication
        # the_user = authenticate(username=username, password=password)
        # if not the_user:
        #     raise forms.ValidationError("Invalid credentials")

        # Approach 2: Checking authentication
        user_obj = User.objects.filter(username=username).first()
        if not user_obj:
            raise forms.ValidationError("Invalid credentials")
        else:
            if not user_obj.check_password(password):
                # log auth tries
                raise forms.ValidationError("Invalid credentials")
        return super(UserLoginForm, self).clean(*args, **kwargs)

    # def clean_username(self):
    #     username = self.cleaned_data.get('username')
    #     user_qs = User.objects.filter(username=username)
    #     user_exists = user_qs.exists()
    #     if not user_exists and user_qs.count() != 1:
    #         raise forms.ValidationError("Invalid credentials")
    #     return username


class UserRegisterForm(forms.ModelForm):
    """
    A form for creating new users. Includes all the required
    fields, plus a repeated password.
    """
    password = forms.CharField(label='Password', widget=forms.PasswordInput)
    password_confirmation = forms.CharField(label='Confirm password', widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = (
            'username',
            'email',
        )

    def clean_password_conf(self):
        """
        Check that the two password entries match
        :param args:
        :param kwargs:
        :return:
        """
        password = self.cleaned_data.get('password')
        password_confirmation = self.cleaned_data.get('password_confirmation')
        if password and password_confirmation and password != password_confirmation:
            raise forms.ValidationError("Passwords don't match")
        return password_confirmation

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(UserRegisterForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user
